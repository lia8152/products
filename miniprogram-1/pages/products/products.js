// pages/products/products.js

const appInstance = getApp();
Page({

  /**
   * Page initial data
   */
  data: {
    products:[{"id": 1 ,"name":"MISS DIOR EAU DE PARFUM" ,"price":579 ,"description":"Miss Dior Eau de Parfum reinvents itself with a new scent.In 1947, Miss Dior was born out of a wild impetus where the pressing desire to re-enchant the lives of women and open up their eyes, once again, to the sparkling colors of love was felt. Following a bleak period, at that time this scent was synonymous with renewed happiness, poetry, and harmony for Christian Dior. Miss Dior intrigues, excites and invites us to marvel at love and all the beauty in the world.","img":'https://eco-beauty.dior.com/dw/image/v2/BDGF_PRD/on/demandware.static/-/Sites-master_dior/default/dw1fdfa7e6/assets/Y0996347/Y0996347_C099600764_E01_ZHC.jpg'},
    {"id": 2 ,"name":"SMALL DIOR BOOK TOTE " ,"price":400 ,"description":"Introduced by Maria Grazia Chiuri, Creative Director of Christian Dior, the Dior Book Tote has become a staple of the Dior aesthetic. Designed to hold all the daily essentials, the style is fully embroidered with a blue Dior Oblique motif. Adorned with the 'CHRISTIAN DIOR PARIS' signature on the front, the small tote exemplifies the House's signature savoir-faire and may be carried by hand.","img":'https://wwws.dior.com/couture/ecommerce/media/catalog/product/cache/1/cover_image_1/870x580/17f82f742ffe127f42dca9de82fb58b1/n/1/1643104834_M1265ZRIW_M928_E01_ZHC.jpg?imwidth=870'},
    {"id": 3 ,"name":"DIOR BACKSTAGE GLOW FACE PALETTE" ,"price":248 ,"description":"Multi-use illuminating makeup palette - highlight and blush","img":'https://cdn.shopify.com/s/files/1/0362/8305/6267/products/Y0015002_C001500004_E01_ZHC_1024x1024@2x.jpg?v=1605275780'},
    {"id": 4 ,"name":"ULTRA DIOR COUTURE PALETTE" ,"description":"The Dior Essentials in a FACE, EYES, and LIPS multi-look palette for couture makeup with custom results. With travel size mascara 3 professional applicators included","price":550 ,"img":'https://eco-beauty.dior.com/dw/image/v2/BDGF_PRD/on/demandware.static/-/Sites-master_dior/default/dw385f41e6/assets/Y0996286/Y0996286_C099600286_E01_ZHC.jpg?sw=870&sh=580&sm=fit&imwidth=870'}],
    counter:0
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    const {
      productsCounter} = appInstance.globalData;
      console.log(productsCounter);
      this.setData({
        counter:productsCounter});
  },

  ontap(event) {
    console.log(event.currentTarget.dataset.product)
    wx.setStorage({ key: 'product', data: event.currentTarget.dataset.product,
     success: function (res) {
        console.log(res)
     }
   })
    wx.navigateTo({
      url: '/pages/productDetailes/productDetailes'
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {
  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {
  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {
  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {
  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
  }
})