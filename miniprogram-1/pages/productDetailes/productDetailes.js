// pages/productDetailes/productDetailes.js

const appInstance = getApp();
Page({

  /**
   * Page initial data
   */
  data: {
    product:{},
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(optins) {
    var that = this;
    wx.getStorage({
      key: 'product',
      success: function (res) {
        console.log(res)
        that.setData({
          product: res.data,
        })
      }
    })
  },

  ontap(event) {
    const {
      productsCounter} = appInstance.globalData;
      appInstance.globalData.productsCounter = productsCounter+1;
      wx.navigateBack({
        delta: 1
      })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {

  }
})