// components/product/product.js
Component({
  /**
   * Component properties
   */
  properties: {
      name: {
      type: String,
      value: 'default value',
    },
    src: {
      type: String,
      value: 'default value',
    },
  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {

  }
})
