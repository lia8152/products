// components/productDetailesComp/productDetailesComp.js
Component({
  /**
   * Component properties
   */
  properties: {
     product:{
        name: {
        type: String,
        value: 'default value',
      },
      img: {
        type: String,
        value: 'default value',
      },
    }
  },

  /**
   * Component initial data
   */
  data: {
  },

  /**
   * Component methods
   */
  methods: {

  }
})
